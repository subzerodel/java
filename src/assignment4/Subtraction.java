package assignment4;

public class Subtraction implements Expression {
    private Expression x;
    private Expression y;

    public Subtraction(Expression x, Expression y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public int interpreter() {
        return x.interpreter() - y.interpreter();
    }
}