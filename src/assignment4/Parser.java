package assignment4;

import java.util.Stack;

public class Parser {
    public static int Parse(String expression) {
        Stack stack = new Stack();
        String[] array = expression.split(" ");

        for (int i = 0; i < array.length; i++) {
            if (isOperator(array[i])) {
                Expression a = (Expression) stack.pop();
                Expression number = new Number(array[i + 1]);
                stack.push((number));
                Expression b = (Expression) stack.pop();
                Expression operator = getOperatorInstance(array[i], a, b);
                i = i + 1;
                int result = operator.interpreter();
                stack.push(new Number(result));
            } else {
                Expression number = new Number(array[i]);
                stack.push(number);
            }
        }
        return (((Expression) stack.pop()).interpreter());
    }

    public static boolean isOperator(String string) {
        if (string.equals("+") || string.equals("-") || string.equals("*")|| string.equals("/")) {
            return true;
        } else {
            return false;
        }
    }


    public static Expression getOperatorInstance(String str, Expression x, Expression y) {
         switch (str) {
            case "+" :
                return new Addition(x, y);
            case "-" :
                return new Subtraction(x, y);
            case "*" :
                return new Multiplication(x, y);
            case "/" :
                return new Division(x,y);
            default : return null;
        }
    }

    public static void main(String[] args) {
        String expression = "32/4+5*2";
        System.out.println(Parse(expression));
    }
}

