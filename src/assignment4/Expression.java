package assignment4;

public interface Expression {
    int interpreter();
}
